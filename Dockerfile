FROM php:7.1-cli

RUN apt-get update && \
    apt-get install -y git zip unzip mysql-client cron  && \
    php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer && \
    apt-get -y autoremove && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY . /zintay
WORKDIR /zintay

RUN composer install

#Cron configuration
ADD cron-agent-dumper /etc/cron.d/cron-agent-dumper
RUN chmod 0644 /etc/cron.d/cron-agent-dumper
CMD touch /tmp/cron.log && cron && tail -f /tmp/cron.log
