.PHONY: build
build:
	docker build . -t zintay-agent

.PHONY: up
up:
	docker run -it --name zintay-agent -v `pwd`/configuration/config.json:/zintay/configuration/config.json zintay-agent

.PHONY: shell
shell:
	docker run -it zintay-agent /bin/bash

.PHONY: stop
clean:
	docker down -v
