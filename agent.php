<?php

require 'vendor/autoload.php';

use Aws\S3\S3Client;


function makeStorageDump(array $config, $sourceFileNamePath)
{
    echo 'Starting storage source dump' . PHP_EOL;
    $mysqldump = "mysqldump -u {$config['user']} --password={$config['password']} --host={$config['host']} ";
    if (isset($config['ssl'])) {
        $mysqldump .= "--ssl-key /zintay/configuration/{$config['ssl']['key']} ";
        $mysqldump .= "--ssl-cert /zintay/configuration/{$config['ssl']['cert']} ";
        $mysqldump .= "--ssl-ca /zintay/configuration/{$config['ssl']['ca']} ";
    }
    $mysqldump .= "{$config['database']} > $sourceFileNamePath";
    exec($mysqldump);
}

function uploadFileToStorage($region, $aws_key, $aws_secret, $bucket, $path, $destinationFilename, $sourceFilePath)
{
    echo "Put dump into storage destination" . PHP_EOL;
    $s3 = new S3Client([
        'version'     => 'latest',
        'region'      => $region,
        'credentials' => [
            'key'    => $aws_key,
            'secret' => $aws_secret,
        ],
    ]);

    $result = $s3->putObject(array(
        'Bucket'     => $bucket,
        'Key'        => $path . $destinationFilename,
        'SourceFile' => $sourceFilePath,
    ));
    return $result;
}

function deleteStoredFile($sourceFileNamePath)
{
    echo "Delete stored file";
    exec('rm ' . $sourceFileNamePath);
}


$json   = file_get_contents('/zintay/configuration/config.json');
$config = json_decode($json, true);


$sourceFileNamePath = "/tmp/backup-" . date("Y-m-d") . ".sql";
makeStorageDump(
    $config['storage_source'],
    $sourceFileNamePath
);

$destinationFilename = "backup-" . date("Y-m-d") . ".sql";
$result              = uploadFileToStorage(
    $config['storage_destination']['region'],
    $config['storage_destination']['aws-key'],
    $config['storage_destination']['aws-secret'],
    $config['storage_destination']['bucket'],
    $config['storage_destination']['path'],
    $destinationFilename,
    $sourceFileNamePath
);
echo $result;

deleteStoredFile($sourceFileNamePath);